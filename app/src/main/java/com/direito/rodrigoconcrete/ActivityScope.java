package com.direito.rodrigoconcrete;

import javax.inject.Scope;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by rodrigo on 02/10/2015.
 *
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface ActivityScope {
}