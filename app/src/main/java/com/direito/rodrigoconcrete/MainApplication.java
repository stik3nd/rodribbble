package com.direito.rodrigoconcrete;

import android.app.Application;
import android.content.Context;
import com.squareup.leakcanary.LeakCanary;

/**
 *
 * Created by rodrigo on 29/09/2015.
 */
public class MainApplication extends Application {

    private RodrigoConcreteComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        init();
        setupGraph();

        if (BuildConfig.DEBUG)
            initDebugTools();
    }

    private void init() {
//        DaoManager.init(this);
//        UserManager.init();
    }

    private void initDebugTools() {
//        Stetho.initialize(
//                Stetho.newInitializerBuilder(this)
//                        .enableDumpapp(
//                                Stetho.defaultDumperPluginsProvider(this))
//                        .enableWebKitInspector(
//                                Stetho.defaultInspectorModulesProvider(this))
//                        .build());
        StethoInjector.injectStetho(this);
        LeakCanary.install(this);
    }

    /**
     * The object graph contains all the instances of the objects
     * that resolve a dependency
     * */
    private void setupGraph() {
        component = DaggerRodrigoConcreteComponent.builder()
                .rodrigoConcreteModule(new RodrigoConcreteModule(this))
                .build();
    }

    public RodrigoConcreteComponent getComponent() {
        return component;
    }

    public static MainApplication getApp(Context context) {
        return (MainApplication) context.getApplicationContext();
    }
}
