package com.direito.rodrigoconcrete;

import android.content.Context;
import com.direito.rodrigoconcrete.interactor.ShotsInteractor;
import com.direito.rodrigoconcrete.module.InteractorModule;
import dagger.Component;

import javax.inject.Singleton;

/**
 * Created by rodrigo on 02/10/2015.
 *
 * <p>
 *     The component due is provide methods that the object graph can use to inject dependencies.
 *     Its like an API for our object graph. <br>
 *
 *     Those methods inject objects created on corresponding modules.
 * </p>
 */

@Singleton
@Component(
        modules = {
                RodrigoConcreteModule.class,
                InteractorModule.class
        }
)
public interface RodrigoConcreteComponent {

    Context getContext();
    ShotsInteractor getShotsInteractor();
}
