package com.direito.rodrigoconcrete.module;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import com.direito.rodrigoconcrete.interactor.ShotsInteractor;
import com.direito.rodrigoconcrete.presenter.ShotsPresenter;
import com.direito.rodrigoconcrete.ui.adapter.ShotsAdapter;
import com.direito.rodrigoconcrete.ui.viewmodel.ShotsView;
import dagger.Module;
import dagger.Provides;

/**
 * Created by rodrigo on 02/10/2015.
 *
 * This module create the object instances for {@link com.direito.rodrigoconcrete.ui.activity.ShotsActivity}.
 * The instances will be injected trough methods declared in {@link com.direito.rodrigoconcrete.component.ShotsComponent}
 */

@Module
public class ShotsModule {

    private ShotsView view;
    private RecyclerView recyclerView;

    public ShotsModule(ShotsView view, RecyclerView recyclerView) {
        this.view = view;
        this.recyclerView = recyclerView;
    }

    @Provides
    public ShotsView provideView() {
        return view;
    }

    @Provides
    public ShotsPresenter providePresenter(ShotsView view, ShotsInteractor interactor) {
        return new ShotsPresenter(view, interactor);
    }

    @Provides
    public RecyclerView provideRecyclerView() {
        return recyclerView;
    }

    @Provides
    public ShotsAdapter provideAdapter(Context context) {
        return new ShotsAdapter(context, recyclerView);
    }
}
