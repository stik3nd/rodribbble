package com.direito.rodrigoconcrete.module;


import com.direito.rodrigoconcrete.interactor.ShotsInteractor;
import com.direito.rodrigoconcrete.io.DribbbleApiService;
import dagger.Module;
import dagger.Provides;

/**
 * Created by rodrigo on 02/10/2015.
 *
 *<p>
 *     This module create the interactors instances. The instances will be injected trough methods
 *     declared in {@link com.direito.rodrigoconcrete.RodrigoConcreteComponent}
 *</p>
 */

@Module
public class InteractorModule {

    @Provides
    public ShotsInteractor provideShotsInteractor(DribbbleApiService apiService){
        return new ShotsInteractor(apiService);
    }
}
