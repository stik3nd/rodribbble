package com.direito.rodrigoconcrete.common;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.ButterKnife;
import com.direito.rodrigoconcrete.MainApplication;
import com.direito.rodrigoconcrete.RodrigoConcreteComponent;


/**
 * <p>
 * Irá executar apenas operações que afetam UI e definidas num view model e acionadas pelo presenter.
 * </p>
 */
public abstract class BaseFragment extends Fragment {

    protected Context CONTEXT;

//    @Override
//    public void onAttach(Activity activity) {
//        super.onAttach(activity);
//        CONTEXT = activity;
//    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        CONTEXT = context;
//        CONTEXT = getActivity();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(getFragmentLayout(), container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindViews(view);
        injectDependencies();
    }

    @Override
    public void onStart() {
        super.onStart();
        getPresenter().onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        getPresenter().onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbindViews();
    }


    /**
     * Specify the layout of the fragment to be inflated in the {@link BaseFragment#onCreateView(LayoutInflater, ViewGroup, Bundle)}
     * */
    protected abstract int getFragmentLayout();

    /**
     * @return The presenter attached to the fragment. This must extends from {@link BasePresenter}
     * */
    protected abstract BasePresenter getPresenter();

    private void injectDependencies() {
        setUpComponent(MainApplication.getApp(getActivity()).getComponent());
    }

    /**
     * Replace all the annotated fields with ButterKnife annotations with the proper value
     * */
    private void bindViews(View rootView) {
        ButterKnife.bind(this, rootView);
    }

    private void unbindViews() {
        ButterKnife.unbind(this);
    }

    /**
     * This method will setup the injector and will commit the dependencies injections.
     * */
    protected abstract void setUpComponent(RodrigoConcreteComponent component);

}
