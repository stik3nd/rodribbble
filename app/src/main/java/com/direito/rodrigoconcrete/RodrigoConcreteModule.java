package com.direito.rodrigoconcrete;

import android.app.Application;
import android.content.Context;
import com.direito.rodrigoconcrete.io.DribbbleApiAdapter;
import com.direito.rodrigoconcrete.io.DribbbleApiService;
import dagger.Module;
import dagger.Provides;
import retrofit.Retrofit;

import javax.inject.Singleton;

/**
 * The module due is create objects to solve dependencies
 * trough methods annotated with {@link dagger.Provides} annotation.
 *<p>
 */

@Module
public class RodrigoConcreteModule {

    private MainApplication app;

    public RodrigoConcreteModule(MainApplication app) {
        this.app = app;
    }

    @Provides
    @Singleton public Application provideApplication() {
        return app;
    }

    @Provides
    @Singleton public Context provideContext() {
        return app;
    }

    @Provides
    @Singleton public Retrofit provideRetrofitInstance() {
        return DribbbleApiAdapter.getInstance();
    }

    @Provides
    @Singleton public DribbbleApiService provideSpotifyApiService(Retrofit retrofit){
        return retrofit.create(DribbbleApiService.class);
    }
}
