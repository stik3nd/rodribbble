package com.direito.rodrigoconcrete.ui.adapter;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.direito.rodrigoconcrete.R;
import com.direito.rodrigoconcrete.domain.Shot;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rodrigo on 02/10/2015.
 *
 */
//public class ShotsAdapter extends RecyclerView.Adapter<ShotsAdapter.ViewHolder>{
public class ShotsAdapter extends RecyclerView.Adapter {
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROGRESS = 0;

    ArrayList<Shot> shots;
    ItemClickListener clickListener;
    Context context;
    ColorDrawable colorDrawable;

    // The minimum amount of items to have below your current scroll position
    // before loading more.
    private int visibleThreshold = 1;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;

    public final int defaultPageNumber = 1;
    private int pageNumber = defaultPageNumber;
    private RecyclerView recyclerView;

    public ShotsAdapter(Context context, RecyclerView recyclerView) {
        System.out.println("recyclerView = " + recyclerView == null ? "null" : "not null");

        this.shots = new ArrayList<>();
        this.context = context;
        this.recyclerView = recyclerView;

        colorDrawable = new ColorDrawable(ContextCompat.getColor(context, R.color.accent));
    }

    /**
     * Should call this after the RecyclerView has set the LinearLayoutManager
     */
    public void prepareLoadMore() {
        System.out.println("ShotsAdapter.prepareLoadMore is instance?");
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            System.out.println("ShotsAdapter.prepareLoadMore");

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();

            recyclerView
                    .addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrolled(RecyclerView recyclerView,
                                               int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);
//                            System.out.println("ShotsAdapter.onScrolled");

                            totalItemCount = linearLayoutManager.getItemCount();
                            lastVisibleItem = linearLayoutManager
                                    .findLastVisibleItemPosition();

                            if (!loading
                                    && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                                //End has been reached

                                if (onLoadMoreListener != null) {
                                    addProgressItem();
                                    pageNumber++;
                                    System.out.println("LOAZD MORE " + pageNumber);
                                    onLoadMoreListener.onLoadMore(pageNumber);
                                }
                                loading = true;
                            }

                        }
                    });
                }

    }

    private void addProgressItem() {
        //add null , so the adapter will check view_type and show progress bar at bottom
        shots.add(null);
        notifyItemInserted(getItemCount() - 1);
    }

    public void removeProgressItem() {
        if (getItemCount() > 0) {
            shots.remove(getItemCount() - 1);
            notifyItemRemoved(getItemCount());
        }
    }

    public Shot getItemAtPosition(int position) {
        if (position == -1)
            return null;
        return shots.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        return shots.get(position) != null ? VIEW_ITEM : VIEW_PROGRESS;
    }

    public void setLoaded() {
        loading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(context).inflate(
                    R.layout.list_item_shot, parent, false);

            vh = new ViewHolder(v);
        } else {
            View v = LayoutInflater.from(context).inflate(
                    R.layout.progress_item, parent, false);

            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            Shot currentShot = shots.get(position);

            ((ViewHolder) holder).setTitle(currentShot.getTitle());
            ((ViewHolder) holder).setShotImage(currentShot.getHighestQualityImage(), currentShot.isAnimated());
            ((ViewHolder) holder).setViewCount(currentShot.getViews_count());
        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return shots.isEmpty() ? 0 : shots.size();
    }

    /**
     * Add item in the last index
     *
     * @param shot The item to be inserted
     */
    public void addItem(Shot shot) {
        if (shot == null)
            throw new NullPointerException("The item cannot be null");

        shots.add(shot);
        notifyItemInserted(getItemCount() - 1);
    }

    /**
     * Add item in determined index
     *
     * @param shot    The event to be inserted
     * @param position Index for the new event
     */
    public void addItem(Shot shot, int position) {
        if (shot == null)
            throw new NullPointerException("The item cannot be null");

        if (position < getItemCount() || position > getItemCount())
            throw new IllegalArgumentException("The position must be between 0 and lastIndex + 1");

        shots.add(position, shot);
        notifyItemInserted(position);
    }

    /**
     * Add a bunch of items
     *
     * @param shots Collection to add
     * */
    public void addAll(List<Shot> shots) {
        if (shots == null)
            throw new NullPointerException("The items cannot be null");

        this.shots.addAll(shots);
        notifyItemRangeInserted(getItemCount() - 1, shots.size());
    }

    public void replace(ArrayList<Shot> shots){
        this.shots = shots;
        notifyDataSetChanged();
    }

    /**
     * Delete all the items
     * */
    public void clear() {
        if (!shots.isEmpty()) {
            shots.clear();
            notifyDataSetChanged();
        }
    }

    public void setOnItemClickListener(ItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

//        public static final int IMG_SIZE_DP = 112;

//        public int IMG_SIZE_PX;

        @Bind(R.id.titleTextView)
        TextView title;

        @Bind(R.id.viewCountTextView)
        TextView viewCountTextView;

        @Bind(R.id.shotImageView)
        ImageView shotImageView;

        @Bind(R.id.playImageView)
        ImageView playImageView;

        public ViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (clickListener != null)
                        clickListener.onItemClicked(getAdapterPosition());
                }
            });

//            setupDimensions();
        }

//        private void setupDimensions() {
//            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
//            IMG_SIZE_PX = IMG_SIZE_DP * (metrics.densityDpi / 160);
//        }

        public void setTitle(String title){
            this.title.setText(title);
        }

        public void setViewCount(int viewCount){
            this.viewCountTextView.setText(String.valueOf(viewCount));
        }

        public void setShotImage(String imageUrl, boolean isAnimated) {
//            System.out.println("imageUrl = " + url);

            if (isAnimated) {
                playImageView.setVisibility(View.VISIBLE);
                shotImageView.setColorFilter(ContextCompat.getColor(context, R.color.black_transparent_image), PorterDuff.Mode.SRC_ATOP);
            } else {
                playImageView.setVisibility(View.GONE);
                shotImageView.setColorFilter(null);
            }

            Glide.with(shotImageView.getContext())
                    .load(imageUrl)
                            .asBitmap()
//                .load(Constants.FAKE_IMAGE_URL)
//                                            .override(IMG_SIZE_PX, IMG_SIZE_PX)
                            //                .animate(android.R.anim.fade_in)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .placeholder(colorDrawable)
                    .error(R.drawable.image_not_found)
                    .into(shotImageView);
        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
        }
    }

    /**
     * Created by rodrigo on 01/10/2015.
     *
     * A simple interface to be configured in a RecyclerView
     */
    public interface ItemClickListener{
        /**
         * This method will be invoked when an item from the list be clicked
         * @param position position in the list
         * */
        void onItemClicked(int position);
    }

    public interface OnLoadMoreListener {
        void onLoadMore(int currentPage);
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public boolean isLoadingMore() {
        return loading;
    }
}
