package com.direito.rodrigoconcrete.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.direito.rodrigoconcrete.R;
import com.direito.rodrigoconcrete.domain.Player;
import com.direito.rodrigoconcrete.domain.Shot;

public class ShotDetailActivity extends AppCompatActivity {
    private static final String ARG_SHOT = "shot";
    private Shot shot;
    private Player user;

    @Bind(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbar;

    @Bind(R.id.backdrop)
    ImageView backdropShotImageView;

    @Bind(R.id.progressBar)
    ProgressBar progressBar;

    @Bind(R.id.playerNameTextView)
    TextView playerNameTextView;

    @Bind(R.id.playerImageView)
    ImageView playerImageView;

    @Bind(R.id.shotDescriptionTextView)
    TextView shotDescriptionTextView;

    public static Intent instance(Context c, Shot shot) {
        Intent i = new Intent(c, ShotDetailActivity.class);
        i.putExtra(ARG_SHOT, shot);
        return i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shot_detail);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getIntent().getExtras() != null) {
            shot = (Shot) getIntent().getExtras().getSerializable(ARG_SHOT);
            user = shot.getUser() == null ? null : shot.getUser();
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (shot == null)
            return;

        collapsingToolbar.setTitle(shot.getTitle());
        loadShot();
        loadUser();
    }

    private void loadUser() {
        playerNameTextView.setText(user.getName());
        Glide.with(this)
                .load(user.getAvatar_url())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .fitCenter()
//                .placeholder(R.color.primary_dark)
                .error(R.drawable.ic_person_black_24dp)
                .into(playerImageView);
    }

    private void loadShot() {
        shotDescriptionTextView.setText(Html.fromHtml(shot.getDescription()));

        String url;

        url = shot.getHighestQualityImage();
        System.out.println("url = " + url);

        Glide.with(this)
                .load(url)
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                progressBar.setVisibility(View.GONE);
                                return false;
                            }
                        })
//                .load("https://d13yacurqjgara.cloudfront.net/users/189524/screenshots/2273519/01-layer-isolate_scene_04_02-800x600.gif")
//                        .asGif()
//                        .asBitmap()
//                .load("http://www.keenthemes.com/preview/metronic/theme/assets/global/plugins/jcrop/demos/demo_files/image1.jpg")
//                .load("http://tr1.cbsistatic.com/hub/i/r/2013/09/19/893d30a1-15f8-458d-a7db-786287f1de5a/resize/620x485/b5d8e4189c64f2abddaa25daaacaf310/android_091913.png")
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.color.primary_dark)
                .error(R.drawable.image_not_found)
                .centerCrop()
                .into(backdropShotImageView);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
}
