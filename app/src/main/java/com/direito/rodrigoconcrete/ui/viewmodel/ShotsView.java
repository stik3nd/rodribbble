package com.direito.rodrigoconcrete.ui.viewmodel;

import com.direito.rodrigoconcrete.domain.Shot;

import java.util.ArrayList;

/**
 * Created by rodrigo on 02/10/2015.
 *
 * <p>
 * Model for every UI operations that could occur in {@link com.direito.rodrigoconcrete.ui.activity.ShotsActivity}
 * </p>
 */
public interface ShotsView {

    void init();

    void setupList();

    void setupAdapter();

    void displayFoundShots(ArrayList<Shot> shots);

    void displayFailedSearch();

    void displayNetworkError();

    void displayServerError(String msg);

    void showLoading(); //Show progressBar | hide List/TextEmpty

    void showContent(); //Hide progressBar/TextEmpty | show List

    void showError(); //Hide progressBar/List | show TextEmpty
}
