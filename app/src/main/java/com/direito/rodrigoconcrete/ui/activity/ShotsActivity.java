package com.direito.rodrigoconcrete.ui.activity;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import com.direito.rodrigoconcrete.R;
import com.direito.rodrigoconcrete.RodrigoConcreteComponent;
import com.direito.rodrigoconcrete.common.BaseActivity;
import com.direito.rodrigoconcrete.common.BasePresenter;
import com.direito.rodrigoconcrete.ui.fragment.ShotsFragment;


/**
 * Created by rodrigo on 02/10/2015.
 * */
public class ShotsActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected int getLayout() {
        return R.layout.activity_shots;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupToolbar();
        setupDrawer();
        setupNavigationView(this);

        //select default item from navigation drawer
        selectFeed();
    }

    @Override
    protected BasePresenter getPresenter() {
        return null;
    }

    @Override
    public void setUpComponent(RodrigoConcreteComponent appComponent) {
        //There isn't dependencies
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_feed) {
            selectFeed();
        } else if (id == R.id.nav_profile) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void selectFeed() {
        getSupportFragmentManager().beginTransaction().replace(
                R.id.content,
                new ShotsFragment(),
                ShotsFragment.class.getSimpleName()
        ).commit();
    }
}
