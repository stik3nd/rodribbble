package com.direito.rodrigoconcrete.ui.fragment;

import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.Bind;
import com.direito.rodrigoconcrete.R;
import com.direito.rodrigoconcrete.RodrigoConcreteComponent;
import com.direito.rodrigoconcrete.common.BaseFragment;
import com.direito.rodrigoconcrete.common.BasePresenter;
import com.direito.rodrigoconcrete.component.DaggerShotsComponent;
import com.direito.rodrigoconcrete.domain.Shot;
import com.direito.rodrigoconcrete.module.ShotsModule;
import com.direito.rodrigoconcrete.presenter.ShotsPresenter;
import com.direito.rodrigoconcrete.ui.activity.ShotDetailActivity;
import com.direito.rodrigoconcrete.ui.adapter.ShotsAdapter;
import com.direito.rodrigoconcrete.ui.viewmodel.ShotsView;

import javax.inject.Inject;
import java.util.ArrayList;


/**
 * Created by rodrigo on 02/10/2015.
 */
public class ShotsFragment extends BaseFragment
        implements ShotsView, ShotsAdapter.ItemClickListener, SwipeRefreshLayout.OnRefreshListener {
    private static final String TAG = "ShotsFragment";

    @Inject
    ShotsPresenter mShotsPresenter;

    @Inject
    ShotsAdapter mResultsAdapter;

//    @Bind(R.id.text_search)
//    ClearableEditText mArtistSearchInput;

    @Inject
    @Bind(R.id.list_shots)
    RecyclerView mShotsList;

    @Bind(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefreshLayout;

    @Bind(android.R.id.empty)
    TextView emptyTextView;

    @Bind(R.id.progressBar)
    ProgressBar progressBar;

    protected Handler handler;

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_shots;
    }

    @Override
    protected BasePresenter getPresenter() {
        return mShotsPresenter;
    }

    @Override
    public void init() {
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
//            System.out.println("ShotsFragment.onResume");
        if (mResultsAdapter != null && mResultsAdapter.getItemCount() == 0) {
            mShotsPresenter.loadShots(mResultsAdapter.defaultPageNumber);
        }
    }

    @Override
    protected void setUpComponent(RodrigoConcreteComponent component) {
        //Set the layout manager before creating the Adapter so the RecyclerView can receive the onScrollListener
        DaggerShotsComponent.builder()
                .rodrigoConcreteComponent(component)
                .shotsModule(new ShotsModule(this, mShotsList))
                .build()
                .inject(this);
    }

    @Override
    public void setupList() {
        mShotsList.setHasFixedSize(true);
        mShotsList.setLayoutManager(new LinearLayoutManager(CONTEXT));
        mShotsList.setAdapter(mResultsAdapter);
        mResultsAdapter.prepareLoadMore();
    }

    @Override
    public void setupAdapter() {
        handler = new Handler();
        mResultsAdapter.setOnItemClickListener(this);
        mResultsAdapter.setOnLoadMoreListener(new ShotsAdapter.OnLoadMoreListener() {
            @Override
            public void onLoadMore(final int currentPage) {
                System.out.println("ShotsFragment.onLoadMore");
                System.out.println("currentPage = [" + currentPage + "]");
//                Execute on a handler to avoid touching the mainthread when the list is scrolling
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mShotsPresenter.loadShots(currentPage);
                    }
                }, 0);
            }
        });
    }

//    @OnTextChanged(R.id.text_search)
    public void onQueryChanged(CharSequence query){
        if (query.length() >= 3)
            mShotsPresenter.loadShots(mResultsAdapter.defaultPageNumber);
        else if (query.length() <= 2)
            mResultsAdapter.clear();
    }

    @Override
    public void displayFoundShots(final ArrayList<Shot> shots) {
        if (mResultsAdapter.isLoadingMore()) {
            mResultsAdapter.removeProgressItem();
            //Execute on a handler to avoid touching the mainthread when the list is scrolling
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mResultsAdapter.addAll(shots);
                    mResultsAdapter.notifyDataSetChanged();
                    mResultsAdapter.setLoaded();
                }
            }, 0);

        } else
            mResultsAdapter.replace(shots);
    }

    @Override
    public void displayFailedSearch() {
    }

    @Override
    public void displayNetworkError() {
        Toast.makeText(CONTEXT, R.string.network_error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void displayServerError(String msg) {
        String serverMsg = msg == null || msg.isEmpty() ? getString(R.string.server_error) : msg;
        Toast.makeText(CONTEXT, serverMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoading() {
        if (progressBar == null)
            return;
        progressBar.setVisibility(View.VISIBLE);
        emptyTextView.setVisibility(View.GONE);
        mShotsList.setVisibility(View.GONE);
    }

    @Override
    public void showContent() {
        if (progressBar == null)
            return;
        progressBar.setVisibility(View.GONE);
        emptyTextView.setVisibility(View.GONE);
        mShotsList.setVisibility(View.VISIBLE);
    }

    @Override
    public void showError() {
        if (mResultsAdapter.isLoadingMore())
            mResultsAdapter.setLoaded();
        if (progressBar == null)
            return;
        progressBar.setVisibility(View.GONE);
        emptyTextView.setVisibility(View.VISIBLE);
        mShotsList.setVisibility(View.GONE);
    }

    @Override
    public void onItemClicked(int position) {
        System.out.println("position = " + position);
//        Bundle params = new Bundle();
//        params.putSerializable(ShotDetailActivity.ARG_SHOT, mResultsAdapter.getItemAtPosition(position));
//        Utils.startActivity(PublishActivity.class, params, getActivity());
        startActivity(ShotDetailActivity.instance(CONTEXT, mResultsAdapter.getItemAtPosition(position)));
//        startActivity(new Intent(CONTEXT, ShotDetailActivity.class).putExtras(params));
    }

    @Override
    public void onRefresh() {
        System.out.println("ShotsFragment.onRefresh");
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
                mResultsAdapter.setPageNumber(mResultsAdapter.defaultPageNumber-1);
                mResultsAdapter.clear();
                mShotsPresenter.loadShots(mResultsAdapter.defaultPageNumber);
            }
        }, 0);
    }
}
