package com.direito.rodrigoconcrete.io.callback;


import com.direito.rodrigoconcrete.domain.Shot;

import java.util.ArrayList;

/**
 * Created by rodrigo on 02/10/2015.
 * Main callback to communicate search results to {@link com.direito.rodrigoconcrete.presenter.ShotsPresenter}
 */
public interface ShotsServerCallback extends ServerCallback {

    void onShotsFound(ArrayList<Shot> shots);

    void onFailedSearch();
}
