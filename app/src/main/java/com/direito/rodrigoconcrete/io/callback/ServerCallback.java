package com.direito.rodrigoconcrete.io.callback;


/**
 * Created by rodrigo on 02/10/2015.
 *
 */
public interface ServerCallback {
    void onNetworkError();

    void onServerError(String msg);
}
