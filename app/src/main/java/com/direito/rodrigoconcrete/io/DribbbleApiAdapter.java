package com.direito.rodrigoconcrete.io;

import android.util.Log;
import com.direito.rodrigoconcrete.StethoInjector;
import com.squareup.okhttp.OkHttpClient;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;

/**
 * Created by rodrigo on 02/10/2015.
 * <p/>
 * * This class contains the instance of the retrofit client to be connected with {@link DribbbleApiService}
 */
public class DribbbleApiAdapter {
    private static final String TAG = "DribbbleApiAdapter";
    private static Retrofit RETROFIT;

    public static Retrofit getInstance() {
        //The adapter will be a singleton
        if (RETROFIT == null) {
            OkHttpClient client = new OkHttpClient();
            if (StethoInjector.getStethoNetworkInterceptor() != null) {
                Log.i(TAG, "No it's not ò-ó");
//                client.networkInterceptors().add(new StethoInterceptor());
                client.networkInterceptors().add(StethoInjector.getStethoNetworkInterceptor());
            }

            RETROFIT = new Retrofit.Builder()
                    .client(client)
                    .baseUrl(DribbbleApiConstants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .build();
        }

        return RETROFIT;
    }
}
