package com.direito.rodrigoconcrete.io;


import com.direito.rodrigoconcrete.domain.Shot;
import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

import java.util.ArrayList;

/**
 * Created by rodrigo on 02/10/2015.
 *
 * All methods that will make a request to Dribbble API
 */
public interface DribbbleApiService {

    /**
     *
     * @return List of shots sorted by popular
     */
    @GET(DribbbleApiConstants.SHOTS_URL)
    Call<ArrayList<Shot>> loadShots(@Query(DribbbleApiConstants.PAGE_QUERY) int currentPage);
}
