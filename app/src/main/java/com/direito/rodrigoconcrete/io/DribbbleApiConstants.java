package com.direito.rodrigoconcrete.io;

/**
 * Created by rodrigo on 02/10/2015.
 *
 * Constants used for requests to API
 */
public class DribbbleApiConstants {

    public static final String BASE_URL = "https://api.dribbble.com/";

    public static final String VERSION_PATH = "v1/";
    public static final String SHOTS_PATH = "shots";

    public static final String TYPE_QUERY = "type";
    public static final String QUERY_TO_SEARCH = "q";
    public static final String TOKEN_QUERY = "access_token";
    public static final String QUERY_TO_TOKEN = "94d2ad946531309cc58c5db928d9b93e6fef1182abf91c6586c74fbd28528aab";
    public static final String PAGE_QUERY = "page";
    public static final String PER_PAGE_QUERY = "per_page";
    public static final String QUERY_FOR_PER_PAGE = "25";

    public static final String SHOTS_URL = VERSION_PATH + SHOTS_PATH + "?"
            + TOKEN_QUERY + "=" + QUERY_TO_TOKEN
            + "&" + PER_PAGE_QUERY + "=" + QUERY_FOR_PER_PAGE;
}
