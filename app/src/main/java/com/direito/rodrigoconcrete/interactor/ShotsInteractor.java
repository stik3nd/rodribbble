package com.direito.rodrigoconcrete.interactor;


import com.direito.rodrigoconcrete.domain.Shot;
import com.direito.rodrigoconcrete.io.DribbbleApiAdapter;
import com.direito.rodrigoconcrete.io.DribbbleApiService;
import com.direito.rodrigoconcrete.io.callback.ShotsServerCallback;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;

import java.util.ArrayList;

/**
 * Created by rodrigo on 02/10/2015.
 */
public class ShotsInteractor {

    DribbbleApiService apiService;
    Call<ArrayList<Shot>> call;

    public ShotsInteractor(DribbbleApiService apiService) {
        this.apiService = apiService;
    }

    /**
     * This method causes NetworkOnMainThread, issue: https://github.com/square/okhttp/issues/1592
     * The code below is solves this problem until it`s fixed
     */
    public void cancelCall() {
        if (call != null)
            DribbbleApiAdapter.getInstance().client().getDispatcher().getExecutorService().execute(new Runnable() {
                @Override
                public void run() {
                    call.cancel();
                }
            });
    }

    public void performSearch(final ShotsServerCallback callback, int currentPage) {
        System.out.println("ShotsInteractor.performSearch");
        System.out.println("callback = [" + callback + "], currentPage = [" + currentPage + "]");
        call = apiService.loadShots(currentPage);
        call.enqueue(new Callback<ArrayList<Shot>>() {
            @Override
            public void onResponse(Response<ArrayList<Shot>> response) {
                ArrayList<Shot> shots = response.body();
                if (shots == null) {
                    System.out.println("response.message() = " + response.message());
                    callback.onServerError(response.message());
                    return;
                }

                if (shots.isEmpty() || shots.size() == 0)
                    callback.onFailedSearch();
                else
                    callback.onShotsFound(shots);
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
                callback.onNetworkError();
            }
        });
//        apiService.loadShots()
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Observer<ArrayList<Shot>>() {
//                    @Override
//                    public void onCompleted() {
//
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        e.printStackTrace();
//                        callback.onNetworkError();
//                    }
//
//                    @Override
//                    public void onNext(ArrayList<Shot> shots) {
//                        if (shots.isEmpty() || shots.size() == 0)
//                            callback.onFailedSearch();
//                        else
//                            callback.onShotsFound(shots);
//
//                    }
//                });

//        apiService.loadShots(query, new Callback<ArtistSearchResponse>() {
//            @Override
//            public void onResponse(Response<ArtistSearchResponse> response) {
////                if (response.isSuccess())
//                if (response.body().getArtists().isEmpty())
//                    callback.onFailedSearch();
//
//                else
//                    callback.onShotsFound(response.body().getArtists());
//            }
//
//            @Override
//            public void onFailure(Throwable t) {
//
//            }

//            @Override
//            public void success(ArtistSearchResponse artistSearchResponse, Response response) {
//                if (artistSearchResponse.getArtists().isEmpty())
//                    callback.onFailedSearch();
//
//                else
//                    callback.onShotsFound(artistSearchResponse.getArtists());
//            }

//            @Override
//            public void failure(RetrofitError error) {
//                if (error.getKind() == RetrofitError.Kind.NETWORK)
//                    callback.onNetworkError(error);
//
//                else
//                    callback.onServerError(error);
//            }
//        });

    }

//    public void performSearch(String query, ArtistSearchServerCallback callback) {
//        apiService.loadShots(query)
//                .subscribeOn(Schedulers.newThread())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(artistSearchResponse -> {callback.onShotsFound(artistSearchResponse.getArtists());}
//                        , throwable -> {callback.onFailedSearch();
//                });
//    }
}
