package com.direito.rodrigoconcrete.domain;

import java.io.Serializable;

/**
 * Created by rodrigo on 02/10/2015.
 *
 */
public class DribbbleImage implements Serializable {
    String hidpi;
    String normal;
    String teaser;

    public DribbbleImage() {
    }

    public String getHidpi() {
        return hidpi;
    }

    public void setHidpi(String hidpi) {
        this.hidpi = hidpi;
    }

    public String getNormal() {
        return normal;
    }

    public void setNormal(String normal) {
        this.normal = normal;
    }

    public String getTeaser() {
        return teaser;
    }

    public void setTeaser(String teaser) {
        this.teaser = teaser;
    }
}
