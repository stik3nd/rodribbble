package com.direito.rodrigoconcrete.domain;


import android.support.annotation.Nullable;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by rodrigo on 02/10/2015.
 *
 */
public class Shot implements Serializable {
    int id;
    String title;
    @Nullable
    String description;
    int views_count;
    boolean animated;
    @Nullable Player user;

    @SerializedName("images")
    @Nullable
    DribbbleImage image;

    public Shot() {
    }

    @Nullable
    public DribbbleImage getImage() {
        return image;
    }

    @Nullable
    public String getHighestQualityImage() {
        if (image != null) {
            if (image.getHidpi() != null)
                return image.getHidpi();
            if (image.getNormal() != null)
                return image.getNormal();
            if (image.getTeaser() != null)
                return image.getTeaser();
        }

        return null;
    }

    public int getViews_count() {
        return views_count;
    }

    public void setViews_count(int views_count) {
        this.views_count = views_count;
    }

    public void setImage(@Nullable DribbbleImage image) {
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Nullable
    public String getDescription() {
        return description == null ? "" : description;
    }

    public void setDescription(@Nullable String description) {
        this.description = description;
    }

    public boolean isAnimated() {
        return animated;
    }

    public void setAnimated(boolean animated) {
        this.animated = animated;
    }

    @Nullable
    public Player getUser() {
        return user;
    }

    public void setUser(@Nullable Player user) {
        this.user = user;
    }
}
