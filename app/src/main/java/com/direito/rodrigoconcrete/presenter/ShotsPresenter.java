package com.direito.rodrigoconcrete.presenter;


import com.direito.rodrigoconcrete.common.BasePresenter;
import com.direito.rodrigoconcrete.domain.Shot;
import com.direito.rodrigoconcrete.interactor.ShotsInteractor;
import com.direito.rodrigoconcrete.io.callback.ShotsServerCallback;
import com.direito.rodrigoconcrete.ui.viewmodel.ShotsView;

import java.util.ArrayList;

/**
 * Created by rodrigo on 02/10/2015.
 *
 */
public class ShotsPresenter extends BasePresenter implements ShotsServerCallback {
    private static final String TAG = "ShotsPresenter";

    ShotsView shotsView;
    ShotsInteractor shotsInteractor;

    public ShotsPresenter(ShotsView view, ShotsInteractor interactor) {
        shotsView = view;
        shotsInteractor = interactor;
    }

    @Override
    public void onStart() {
//        System.out.println("ShotsPresenter.onStart");
        shotsView.init();
        shotsView.setupAdapter();
        shotsView.setupList();
    }


    @Override
    public void onStop() {
        shotsInteractor.cancelCall();
    }

    public void loadShots(int currentPage) {
        if (currentPage == 1)
            shotsView.showLoading();
        shotsInteractor.performSearch(this, currentPage);
    }

    @Override
    public void onShotsFound(ArrayList<Shot> shots) {
        shotsView.showContent();
        shotsView.displayFoundShots(shots);
    }

    @Override
    public void onFailedSearch() {
        shotsView.showError();
        shotsView.displayFailedSearch();
    }

    @Override
    public void onNetworkError() {
        shotsView.showError();
        shotsView.displayNetworkError();
    }

    @Override
    public void onServerError(String msg) {
        shotsView.showError();
        shotsView.displayServerError(msg);
    }
}
