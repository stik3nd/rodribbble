package com.direito.rodrigoconcrete.component;


import com.direito.rodrigoconcrete.ActivityScope;
import com.direito.rodrigoconcrete.RodrigoConcreteComponent;
import com.direito.rodrigoconcrete.module.ShotsModule;
import com.direito.rodrigoconcrete.presenter.ShotsPresenter;
import com.direito.rodrigoconcrete.ui.adapter.ShotsAdapter;
import com.direito.rodrigoconcrete.ui.fragment.ShotsFragment;
import dagger.Component;

/**
 * Created by rodrigo on 02/10/2015.
 *
 * <p>
 *     Methods to inject dependencies into {@link com.direito.rodrigoconcrete.ui.activity.ShotsActivity}
 * </p>
 */

@ActivityScope
@Component(
        dependencies = RodrigoConcreteComponent.class,
        modules = ShotsModule.class
)
public interface ShotsComponent {

    void inject(ShotsFragment shotsFragment);

    ShotsPresenter getPresenter();
    ShotsAdapter getAdapter();
}
