package com.direito.rodrigoconcrete;

import android.app.Application;
import com.facebook.stetho.Stetho;
import com.facebook.stetho.okhttp.StethoInterceptor;
import com.squareup.okhttp.Interceptor;

/**
 * Created by stik on 10/9/15.
 */
public class StethoInjector {
    public static void injectStetho(final Application application) {
        Stetho.initialize(
                Stetho.newInitializerBuilder(application)
                        .enableDumpapp(
                                Stetho.defaultDumperPluginsProvider(application))
                        .enableWebKitInspector(
                                Stetho.defaultInspectorModulesProvider(application))
                        .build());
    }

    public static Interceptor getStethoNetworkInterceptor() {
        return new StethoInterceptor();
    }
}
