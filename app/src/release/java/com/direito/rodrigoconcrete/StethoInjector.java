package com.direito.rodrigoconcrete;

import android.app.Application;
import com.squareup.okhttp.Interceptor;


/**
 * Created by stik on 10/9/15.
 */
public class StethoInjector {
    public static void injectStetho(final Application application) {
        //no-op
    }

    public static Interceptor getStethoNetworkInterceptor() {
        //no-op
        return null;
    }
}
