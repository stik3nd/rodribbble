# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /home/stik/Android/sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

#Gotta keep the names on Domain and Model because the service relies on the correct name when getting the JSON from the service
-keepclasseswithmembers class com.direito.rodrigoconcrete.domain.** {
    *;
}

-keepclasseswithmembers class com.direito.rodrigoconcrete.model.** {
    *;
}

#OkHttp & Okio & Retrofit & RxJava
-dontwarn rx.**

-dontwarn okio.**

-dontwarn com.squareup.okhttp.**
-keep class com.squareup.okhttp.** { *; }
-keep interface com.squareup.okhttp.** { *; }

-dontwarn retrofit.**
-keep class retrofit.** { *; }
-keepclasseswithmembers class * {
    @retrofit.http.* <methods>;
}
-keepattributes Signature
-keepattributes Exceptions
-keepattributes *Annotation*

-keep class com.squares.timevent.api.** { *; }

#Butter Knife
-keep class butterknife.** { *; }
-dontwarn butterknife.internal.**
-keep class **$$ViewBinder { *; }

-keepclasseswithmembernames class * {
    @butterknife.* <fields>;
}

-keepclasseswithmembernames class * {
    @butterknife.* <methods>;
}